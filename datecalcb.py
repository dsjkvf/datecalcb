#!/usr/bin/env python

# HEADER

# Details
__title__ = ""
__author__ = "dsjkvf"
__maintainer__ = "dsjkvf"
__email__ = "dsjkvf@gmail.com"
__copyright__ = "dsjkvf"
__credits__ = []
__license__ = "GPL"
__version__ = "1.0.1"
__status__ = "Production"

# GLOBAL VARIABLES

# Settings
# external program to play an alarm as the countdown stops
the_plyr = '/usr/bin/afplay'
# external file to be played as an alarm
the_alrm = '/Users/s/Music/Alerts/Attention.wav'

# MODULES

import readline
import datetime
import time
import sys
import subprocess
import thread

# HELPERS

# Show help
def show_help():
    print "Press 'a' to add a delta to the date of your choice"
    print "Press 'd' to calculate the differnce between the current date and and the date of your choice"
    print "Press 'b' to calculate the differnce between the two dates of your choice"
    print "Press 'w' to report a weekday for the date of your choice"
    print "Press 't' to calculate the differnce between the current time and the time of your choice"
    print "Press 's' to start a stopwatch"
    print "Press 'c' to start a countdown"
    print "Press 'q' to quit"

# Add a delta a to date
def add_days():
    the_day = raw_input("Enter the date [%s]: " % datetime.date.today()) or str(datetime.date.today())
    try:
        the_day = datetime.datetime.strptime(the_day, "%Y-%m-%d")
    except:
        try:
            the_day = datetime.datetime.strptime(the_day, "%Y%m%d")
        except:
            return "Wrong data entered"
    the_del = raw_input("Enter the delta [50]: ") or "50"
    try:
        the_del = int(the_del)
    except:
        return "Wrong data entered"
    the_day = the_day + datetime.timedelta(days = the_del)
    return the_day.date()

# Diff a date
def diff_days():
    the_day = raw_input("Enter the date [%s]: " % datetime.date.today()) or str(datetime.date.today())
    try:
        the_day = datetime.datetime.strptime(the_day, "%Y-%m-%d")
    except:
        try:
            the_day = datetime.datetime.strptime(the_day, "%Y%m%d")
        except:
            return "Wrong data entered"
    the_del = the_day - datetime.datetime.today()
    if datetime.datetime.now().time() != datetime.time(hour=0, minute=0, second=0, microsecond=0):
        return the_del.days + 1
    else:
        return the_del.days

# Diff between two dates
def diff_two_days():
    the_day1 = raw_input("Enter the date [%s]: " % datetime.date.today()) or str(datetime.date.today())
    the_day2 = raw_input("Enter the date [%s]: " % datetime.date.today()) or str(datetime.date.today())
    try:
        the_day1 = datetime.datetime.strptime(the_day1, "%Y-%m-%d")
        the_day2 = datetime.datetime.strptime(the_day2, "%Y-%m-%d")
    except:
        try:
            the_day1 = datetime.datetime.strptime(the_day1, "%Y%m%d")
            the_day2 = datetime.datetime.strptime(the_day2, "%Y%m%d")
        except:
            return "Wrong data entered"
    the_del = the_day2 - the_day1
    return the_del.days

# Show the day of the week
def show_weekday():
    the_day = raw_input("Enter the date [%s]: " % datetime.date.today()) or str(datetime.date.today())
    wdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
    try:
        the_day = datetime.datetime.strptime(the_day, "%Y-%m-%d").weekday()
        return wdays[the_day]
    except:
        try:
            the_day = datetime.datetime.strptime(the_day, "%Y%m%d").weekday()
            return wdays[the_day]
        except:
            return "Wrong data entered"

# Diff times
def diff_hours():
    # get another user input
    the_time = raw_input(
            "Enter the time [%s:%s]: " % (
                '{:02d}'.format(datetime.datetime.now().hour),
                '{:02d}'.format(datetime.datetime.now().minute)
                )) or (str(datetime.datetime.now().hour) +
                str(datetime.datetime.now().minute))
    # translate it into datetime object
    try:
        the_time = datetime.datetime.combine(datetime.date(1, 1, 1),
                datetime.datetime.strptime(the_time, "%H:%M").time())
    except:
        try:
            the_time = datetime.datetime.combine(datetime.date(1, 1, 1),
                    datetime.datetime.strptime(the_time, "%H%M").time())
        except:
            return "Wrong data entered"
    # translate current time into datetime object
    the_curr = datetime.datetime.combine(datetime.date(1, 1, 1),
            datetime.datetime.today().time().replace(second=0, microsecond = 0))
    # calculate the delta
    the_delt = the_time - the_curr
    # extract hours...
    dlt_h = (the_delt.seconds//3600)
    # ...and minutes
    dlt_m = (the_delt.seconds//60)%60
    # gather everyting into the rezult
    dlt = '{:02d}:{:02d}'.format(dlt_h, dlt_m)
    # but inspect a few corner cases
    # 1. if the time has already passed
    if the_delt.days < 0:
        #2. if the minutes part equals 0 (would be printed out as 60, must be avoided)
        if dlt_m == 0:
            dlt_m = '00'
            #3. besides, if the hours part equals 23, too (would be printed out as 00:00, must be avoided)
            if dlt_h == 23:
                dlt_h = '01'
        else:
            dlt_m = '{:02d}'.format(60 - dlt_m)
            dlt_h = '{:02d}'.format(24 - dlt_h - 1)
        # add this negative delta to the rezult
        dlt = dlt + '; -' + dlt_h + ':' + dlt_m
    # return the rezult
    return dlt

# Introduce a controlling thread (to use for aborting a stopwatch or a countdown)
def control_thread(control_list):
    raw_input()
    control_list.append(True)

# Start a stopwatch
def stopwatch():
    the_time = 0
    control_list = []
    thread.start_new_thread(control_thread, (control_list,))
    print "Press ENTER to stop:"
    while not control_list:
        the_time += 1
        m, s = divmod(the_time, 60)
        h, m = divmod(m, 60)
        timeformat = '{:02d}:{:02d}:{:02d}'.format(h, m, s)
        print timeformat, '\r',
        sys.stdout.flush()
        time.sleep(1)

# Start a countdown
def countdown(the_time):
    # translate string into int
    try:
        the_time = int(the_time) * 60
    except:
        return 'Wrong data entered'
    control_list = []
    thread.start_new_thread(control_thread, (control_list,))
    print "Press ENTER to stop:"
    # https://stackoverflow.com/a/25189629/
    while the_time:
        if control_list:
            return
        m, s = divmod(the_time, 60)
        h, m = divmod(m, 60)
        timeformat = '{:02d}:{:02d}:{:02d}'.format(h, m, s)
        # https://stackoverflow.com/q/2456148/
        print timeformat, '\r',
        sys.stdout.flush()
        time.sleep(1)
        the_time -= 1
    return subprocess.call([the_plyr, the_alrm])

# MAIN

# Define
def main():
    while True:
        q = raw_input('Enter the minutes for a countdown [5] or a command: ') or '5'
        if q == "q" or q == "quit":
            quit()
        elif q =="h" or q == "help":
            show_help()
        elif q =="a" or q == "add":
            print add_days()
        elif q =="d" or q == "days":
            print diff_days()
        elif q =="b" or q == "between":
            print diff_two_days()
        elif q =="w" or q == "weekday":
            print show_weekday()
        elif q =="t" or q == "times":
            print diff_hours()
        elif q =="s" or q == "stopwatch":
            stopwatch()
        # countdown() is put under 'else' clause since it's the default action
        else:
            countdown(q)
    return

# Run
while True:
    if __name__ == "__main__":
        main()
