datecalcb
=========

## About

`datecalcb` is a simple interactive Python script, which is capable of the following:

  - calculating the resulting date after adding a number of days to the current date
  - calculating the difference between the current date and the date of user's choice
  - calculating the difference between the two dates of user's choice
  - reporting the weekday for the date of user's choice
  - calculating the difference between the current time and the time of user's choice
  - starting a stopwatch
  - starting a countdown (and playing a sound as the countdown reaches zero)

## Usage

As `datecalcb` is launched, it awaits either for a number of minutes to start a countdown (this is its default mode), or for a command to switch to another mode. The following commands are available:

  - `h` to show help
  - `a` to add a delta to the current date
  - `d` to calculate the difference between the current date and the date of your choice
  - `b` to calculate the difference between the two dates of your choice"
  - `w` to report a weekday for the date of your choice"
  - `t` to calculate the difference between the current time and the time of your choice"
  - `s` to start a stopwatch"
  - `c` to start a countdown"
  - `q` to quit a program

IMPORTANT: dates and times can be entered without any separators, simply as `19990101` or `1545`.

## Disclaimer

Having said all that, I want to accentuate that `datecalcb` is still a extremely limited script, and I strongly encourage you to use the famous [`pdd`](https://github.com/jarun/pdd) instead.
